package com.gedia.base.di

import com.gedia.base.utils.NetworkHelper
import org.koin.dsl.module

/**
 * Koin utils module
 */
val utilsModule = module {


    single { NetworkHelper() }
}
