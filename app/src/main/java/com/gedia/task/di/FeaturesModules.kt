package com.gedia.task.di

import com.gedia.userdetails.di.userDetailsModule
import com.gedia.userslist.di.usersListModule


var allFeatures = listOf(
    usersListModule,
    userDetailsModule
)
