package com.gedia.task.app

import androidx.multidex.MultiDexApplication
import com.gedia.base.di.utilsModule
import com.gedia.data.di.databaseModule
import com.gedia.data.di.networkModule
import com.gedia.data.di.repositoryModule
import com.gedia.task.di.allFeatures
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

/**
 * Custom app class to initialize koin dependencies graph
 */
class GediaApp : MultiDexApplication() {


    override fun onCreate() {
        super.onCreate()
        initKoin()
    }


    private fun initKoin() {
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@GediaApp)
            val modules = ArrayList(allFeatures)
            modules.addAll(listOf(databaseModule, utilsModule ,networkModule, repositoryModule))
            modules(
                modules
            )
        }
    }


}