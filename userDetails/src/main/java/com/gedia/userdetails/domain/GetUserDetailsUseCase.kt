package com.gedia.userdetails.domain

import com.gedia.data.repository.users.UsersRepo


open class GetUserDetailsUseCase constructor(private val repository: UsersRepo) {

    suspend operator fun invoke(id:Int) = repository.getUserDetails(id)

}