package com.gedia.userdetails.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.lifecycle.MutableLiveData
import java.util.*


class CounterService : Service() {
    private val binder = LocalBinder()


    val countLiveData = MutableLiveData<Long>()


    override fun onCreate() {
        super.onCreate()
        startForeground()
        startTimer()
    }

    private fun startTimer() {
        object : CountDownTimer(5 * 1000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                countLiveData.value = millisUntilFinished / 1000
            }

            override fun onFinish() {
                stopSelf()
            }
        }.start()
    }

    inner class LocalBinder : Binder() {
        fun getService(): CounterService = this@CounterService
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }


    private fun startForeground() {
        if (Build.VERSION.SDK_INT >= 26) {
            val CHANNEL_ID = "my_channel_01"
            val channel = NotificationChannel(
                CHANNEL_ID,
                "Counter service",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(
                channel
            )
            val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("")
                .setContentText("").build()
            startForeground(1, notification)
        }
    }

}
