package com.gedia.userdetails.di



import com.gedia.userdetails.domain.GetUserDetailsUseCase
import com.gedia.userdetails.view.UserDetailsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val userDetailsModule = module {


    factory { GetUserDetailsUseCase(get()) }

    viewModel {(id:Int) -> UserDetailsViewModel(id,get()) }
}