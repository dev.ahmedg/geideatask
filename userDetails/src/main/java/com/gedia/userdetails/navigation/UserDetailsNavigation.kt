package com.gedia.userdetails.navigation

import android.os.Bundle
import com.gedia.base.baseEntities.BaseNavigationManager
import com.gedia.userdetails.view.UserDetailsActivity

class UserDetailsNavigation : BaseNavigationManager() {

    fun startDetailsScreen(id:Int){
        startWithBundle(UserDetailsActivity::class.java, Bundle().apply {
            putInt(KEY_ID,id)
        })
    }
}