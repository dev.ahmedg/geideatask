package com.gedia.userdetails.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gedia.data.network.entities.UserItem
import com.gedia.data.other.ResultState
import com.gedia.userdetails.domain.GetUserDetailsUseCase
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class UserDetailsViewModel(private val id:Int, private val getUserDetailsUseCase: GetUserDetailsUseCase): ViewModel() {


    private val _detailsLiveData = MutableLiveData<ResultState<UserItem>>()
    val detailsLiveData : LiveData<ResultState<UserItem>> = _detailsLiveData

    init {
        getData()
    }

    fun getData() {
        viewModelScope.launch {
            getUserDetailsUseCase(id).collect {
                _detailsLiveData.value = it
            }
        }
    }
}