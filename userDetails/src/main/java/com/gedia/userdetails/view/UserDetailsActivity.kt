package com.gedia.userdetails.view

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import androidx.core.content.ContextCompat
import com.gedia.base.baseEntities.BaseActivity
import com.gedia.base.baseEntities.BaseNavigationManager.Companion.KEY_ID
import com.gedia.base.utils.bindImage
import com.gedia.data.network.entities.UserItem
import com.gedia.data.other.ResultState
import com.gedia.userdetails.R
import com.gedia.userdetails.databinding.ActivityUserDetailsBinding
import com.gedia.userdetails.services.CounterService
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class UserDetailsActivity : BaseActivity<ActivityUserDetailsBinding>() {

    private val userId by lazy { intent.getIntExtra(KEY_ID,0) }

    private val mViewModel by viewModel<UserDetailsViewModel>{
        parametersOf(userId)
    }


    private lateinit var mService: CounterService
    private var mBound: Boolean = false

    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as CounterService.LocalBinder
            mService = binder.getService()
            mBound = true
            observeCounter()
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mBound = false
        }
    }

    override fun getLayoutRes(): Int = R.layout.activity_user_details

    override fun getToolbarTitle(): Any = "#$userId"

    override fun onViewAttach() {
        super.onViewAttach()
        observeDetails()
        startService()
    }



    private fun observeDetails() {
        mViewModel.detailsLiveData.observe(this){
            when(it){
                is ResultState.Success -> populateData(it.data!!)
                is ResultState.Loading ->showLoading(mBinding.stateful)
                is ResultState.Error -> showError(mBinding.stateful,it.exception.message){mViewModel.getData()}
                else -> {}
            }
        }

    }

    private fun observeCounter() {

        mService.countLiveData.observe(this, {
            if (it == 0L)finish()
            else mBinding.tvCounter.text = it.toString()
        })
    }

    private fun populateData(data: UserItem) {
        showContent(mBinding.stateful)
        mBinding.tvEmail.text = data.email
        mBinding.tvName.text = data.fullName()
        bindImage(mBinding.ivImage,data.avatar)

    }

    private fun startService(){
        ContextCompat.startForegroundService(this, Intent(this, CounterService::class.java))

    }
    override fun onStart() {
        super.onStart()
        // Bind to LocalService
        Intent(this, CounterService::class.java).also { intent ->
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        super.onStop()
        unbindService(connection)
        mBound = false
    }
}