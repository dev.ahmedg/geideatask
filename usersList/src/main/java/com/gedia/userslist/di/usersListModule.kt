package com.gedia.userslist.di




import com.gedia.userslist.domain.GetUsersUseCase
import com.gedia.userslist.view.UsersListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val usersListModule = module {

    factory { GetUsersUseCase(get()) }


    viewModel { UsersListViewModel(get()) }
}