package com.gedia.userslist.domain

import com.gedia.data.repository.users.UsersRepo


class GetUsersUseCase constructor(private val repository: UsersRepo) {

    operator fun invoke() = repository.getUsers()

}