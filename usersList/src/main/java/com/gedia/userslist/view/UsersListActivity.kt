package com.thirdwayv.gameslist.view

import android.content.Intent
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.lifecycleScope
import com.gedia.base.baseEntities.BaseActivity
import com.gedia.base.baseEntities.LoaderStateAdapter
import com.gedia.base.utils.handleInitialState
import com.gedia.userdetails.navigation.UserDetailsNavigation
import com.gedia.userslist.R
import com.gedia.userslist.databinding.ActivityUsersListBinding
import com.gedia.userslist.view.UserPagedAdapter
import com.gedia.userslist.view.UsersListViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class UsersListActivity : BaseActivity<ActivityUsersListBinding>() {



    private val mViewModel by viewModel<UsersListViewModel>()

    private val gamesPagedAdapter = UserPagedAdapter{
        onItemClick(it.id)
    }

    private fun onItemClick(id: Int) {
        UserDetailsNavigation().startDetailsScreen(id)
    }


    private val gamesLoadStateAdapter = LoaderStateAdapter { gamesPagedAdapter.retry() }

    override fun getLayoutRes(): Int  = R.layout.activity_users_list

    override fun getToolbarTitle(): Any  = R.string.users

    override fun onViewAttach() {
        super.onViewAttach()
        setUpRv()
        observeData()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        gamesPagedAdapter.refresh()
    }

    private fun observeData() {

        lifecycleScope.launch {
            mViewModel.allUsersPaged.distinctUntilChanged().collectLatest {
                gamesPagedAdapter.submitData(it)
            }
        }

        gamesPagedAdapter.handleInitialState(
            onLoading = {showLoading(mBinding.stateful)},
            onEmpty = {showEmpty(mBinding.stateful)},
            onError = {showError(mBinding.stateful,it.message){gamesPagedAdapter.retry()}},
            resetStates = {showContent(mBinding.stateful)}
        )
    }

    private fun setUpRv() {
        mBinding.rv.adapter = gamesPagedAdapter.withLoadStateFooter(gamesLoadStateAdapter)
    }


}