package com.gedia.userslist.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.gedia.base.baseEntities.BasePagedListAdapter
import com.gedia.data.network.entities.UserItem
import com.gedia.userslist.databinding.ItemUserBinding

class UserPagedAdapter(private val onItemClick:(UserItem)->Unit): BasePagedListAdapter<UserItem>() {


    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        return ItemUserBinding.inflate(LayoutInflater.from(parent.context),parent,false)
    }

    override fun bind(binding: ViewDataBinding, position: Int) {
        val item = getItem(position) ?:return
        (binding as ItemUserBinding).apply {
            model = item
            onClick = View.OnClickListener {
                onItemClick(item)
            }
        }
    }
}