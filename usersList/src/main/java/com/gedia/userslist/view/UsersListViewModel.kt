package com.gedia.userslist.view

import androidx.lifecycle.ViewModel
import com.gedia.userslist.domain.GetUsersUseCase


class UsersListViewModel(getUsersUseCase: GetUsersUseCase):ViewModel() {


    val allUsersPaged = getUsersUseCase()

}