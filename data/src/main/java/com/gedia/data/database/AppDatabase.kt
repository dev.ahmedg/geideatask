package com.gedia.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gedia.data.database.dao.UserDao
import com.gedia.data.database.dao.UserRemoteKeyDao
import com.gedia.data.database.entity.UserRemoteKey
import com.gedia.data.network.entities.UserItem

/**
 * Room database class
 */
@Database(
    entities = [UserItem::class, UserRemoteKey::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao() : UserDao
    abstract fun userRemoteKeysDao() : UserRemoteKeyDao


}

