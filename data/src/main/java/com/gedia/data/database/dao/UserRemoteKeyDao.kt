package com.gedia.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gedia.data.database.entity.UserRemoteKey

@Dao
interface UserRemoteKeyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKey: List<UserRemoteKey>)

    @Query("SELECT * FROM game_remote_key WHERE id = :id")
    suspend fun remoteKeyById(id: Int): UserRemoteKey?

    @Query("DELETE FROM game_remote_key")
    suspend fun clearRemoteKeys()
}

