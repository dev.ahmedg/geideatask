package com.gedia.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey



@Entity(tableName = "game_remote_key")
data class UserRemoteKey(@PrimaryKey val id: Int, val prevKey: Int?, val nextKey: Int?)
