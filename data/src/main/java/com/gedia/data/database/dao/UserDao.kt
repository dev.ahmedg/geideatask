package com.gedia.data.database.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gedia.data.network.entities.UserItem

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(models: List<UserItem>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(model: UserItem)

    @Query("SELECT * FROM user")
    fun getAll(): PagingSource<Int, UserItem>

    @Query("DELETE FROM user")
    suspend fun clearAll()

    @Query("SELECT * FROM user WHERE id = :id")
    suspend fun getById(id: Int): UserItem


}