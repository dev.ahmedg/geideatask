package com.gedia.data.network.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.gedia.base.baseEntities.BaseEntity
import com.google.gson.annotations.SerializedName

data class UsersListResponse(

	@field:SerializedName("per_page")
	val perPage: Int? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("data")
	val data: List<UserItem>,

	@field:SerializedName("page")
	val page: Int? = null,

	@field:SerializedName("total_pages")
	val totalPages: Int? = null
)

@Entity(tableName = "user")
data class UserItem(

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@PrimaryKey
	@field:SerializedName("id")
	val id: Int,

	@field:SerializedName("avatar")
	val avatar: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("email")
	val email: String? = null
):BaseEntity {
	override fun entityId(): Int  = id

	fun fullName() = firstName.plus(" ").plus(lastName)
}
