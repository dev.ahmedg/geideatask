package com.gedia.data.network.entities

import com.google.gson.annotations.SerializedName

data class UserDetailsResponse(

	@field:SerializedName("data")
	val data: UserItem
)
