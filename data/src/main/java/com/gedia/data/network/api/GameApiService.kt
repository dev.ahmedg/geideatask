package com.gedia.data.network.api



import com.gedia.data.constant.USER_DETAILS_API
import com.gedia.data.constant.USER_LIST_API
import com.gedia.data.network.entities.UserDetailsResponse
import com.gedia.data.network.entities.UsersListResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GameApiService {

    @GET(USER_LIST_API)
    suspend fun getUsers(@Query("page") page: Int, @Query("per_page") pageSize: Int): Response<UsersListResponse>


    @GET(USER_DETAILS_API)
    suspend fun getUserDetails(@Path("id")id:Int): Response<UserDetailsResponse>


}