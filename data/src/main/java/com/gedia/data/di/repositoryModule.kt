package com.gedia.data.di



import com.gedia.data.repository.users.*
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module


/**
 * Koin module for repositories , remote/local data sources an other sources
 */
val  repositoryModule = module {

    factory<UsersLocalDataSource>{ UsersLocalDataSourceRoomImpl(get()) }

    factory<UsersRemoteDataSource>{ UsersRemoteDataSourceRetrofitImpl(get(),get()) }

    factory<UsersRepo>{ UsersRepoImpl(androidContext(),get(),get(),get()) }


}