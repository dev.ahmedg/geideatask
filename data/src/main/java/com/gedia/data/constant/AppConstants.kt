package com.gedia.data.constant

import androidx.paging.PagingConfig
import com.gedia.data.BuildConfig


var BASE_API_URL = BuildConfig.BASE_API_URL
const val DEFAULT_NETWORK_TIMEOUT = 100L

const val DEFAULT_PAGE_INDEX = 1
const val DEFAULT_PAGE_SIZE = 10



const val USER_LIST_API = "users"
const val USER_DETAILS_API = "users/{id}"

fun getDefaultPageConfig(): PagingConfig {
    return PagingConfig( enablePlaceholders = false,pageSize = DEFAULT_PAGE_SIZE)
}