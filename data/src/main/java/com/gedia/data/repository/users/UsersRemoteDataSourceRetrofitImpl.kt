package com.gedia.data.repository.users

import com.gedia.data.network.RetrofitExecutor
import com.gedia.data.network.api.GameApiService
import com.gedia.data.network.entities.UserItem
import com.gedia.data.network.entities.UsersListResponse


class UsersRemoteDataSourceRetrofitImpl(private val retrofitExecutor: RetrofitExecutor, private val apiService: GameApiService) :
    UsersRemoteDataSource {


    override suspend fun getUsers(page: Int, pageSize: Int): UsersListResponse {
        return retrofitExecutor.makeRequest { apiService.getUsers(page,pageSize) }
    }

    override suspend fun getUserDetails(id: Int): UserItem {
        return retrofitExecutor.makeRequest { apiService.getUserDetails(id) }.data
    }


}