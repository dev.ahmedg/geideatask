package com.gedia.data.repository.users


import androidx.paging.PagingSource
import androidx.room.withTransaction
import com.gedia.data.database.AppDatabase
import com.gedia.data.database.entity.UserRemoteKey
import com.gedia.data.network.entities.UserItem


class UsersLocalDataSourceRoomImpl(private val appDatabase: AppDatabase) : UsersLocalDataSource {


    override fun getUsers(): PagingSource<Int, UserItem> {
        return appDatabase.userDao().getAll()
    }

    override suspend fun saveData(
        isRefresh: Boolean,
        games: List<UserItem>,
        remoteKeys: List<UserRemoteKey>
    ) {
        appDatabase.withTransaction {
            if (isRefresh){
                appDatabase.userDao().clearAll()
                appDatabase.userRemoteKeysDao().clearRemoteKeys()
            }
            appDatabase.userDao().insertAll(games)
            appDatabase.userRemoteKeysDao().insertAll(remoteKeys)
        }
    }

    override suspend fun removeAllUsers() {
        appDatabase.userDao().clearAll()
    }

    override suspend fun getRemoteKeyById(id: Int): UserRemoteKey? {
        return appDatabase.userRemoteKeysDao().remoteKeyById(id)
    }

    override suspend fun getUserDetails(id: Int): UserItem {
        return appDatabase.userDao().getById(id)
    }

    override suspend fun updateUserItem(item: UserItem) {
        appDatabase.userDao().insert(item)
    }


}