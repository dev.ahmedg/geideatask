package com.gedia.data.repository.users

import android.content.Context
import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingData
import com.gedia.base.utils.NetworkHelper
import com.gedia.data.constant.getDefaultPageConfig
import com.gedia.data.network.entities.UserItem
import com.gedia.data.other.RemoteLocalBoundResource
import com.gedia.data.other.ResultState
import kotlinx.coroutines.flow.Flow

/**
 * Games repository implementation
 */
@OptIn(ExperimentalPagingApi::class)
class UsersRepoImpl(
    private val context: Context,
    private val networkHelper: NetworkHelper,
    private val localDataSource: UsersLocalDataSource,
    private val remoteDataSource: UsersRemoteDataSource
) : UsersRepo {


    override fun getUsers(): Flow<PagingData<UserItem>> {
        val pagingSourceFactory = { localDataSource.getUsers()}
        return Pager(
            config = getDefaultPageConfig(),
            pagingSourceFactory = pagingSourceFactory,
            remoteMediator = UsersPagingMediator(context, networkHelper, localDataSource, remoteDataSource)
        ).flow
    }

    override suspend fun getUserDetails(id: Int): Flow<ResultState<UserItem>> {


        return RemoteLocalBoundResource(context,networkHelper,
            remoteCall = { remoteDataSource.getUserDetails(id)},
            localCall = {localDataSource.getUserDetails(id)},
            saveLocal = { localDataSource.updateUserItem(it)}).asFlow()

    }


}