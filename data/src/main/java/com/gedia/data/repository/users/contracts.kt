package com.gedia.data.repository.users

import androidx.paging.PagingData
import androidx.paging.PagingSource
import com.gedia.data.database.entity.UserRemoteKey
import com.gedia.data.network.entities.UserItem
import com.gedia.data.network.entities.UsersListResponse
import com.gedia.data.other.ResultState
import kotlinx.coroutines.flow.Flow


interface UsersLocalDataSource {

    fun getUsers() : PagingSource<Int, UserItem>

    suspend fun saveData(isRefresh: Boolean, games: List<UserItem>, remoteKeys: List<UserRemoteKey>)

    suspend fun removeAllUsers()

    suspend fun getRemoteKeyById(id:Int) : UserRemoteKey?

    suspend fun getUserDetails(id:Int): UserItem

    suspend fun updateUserItem(item: UserItem)
}


interface UsersRemoteDataSource {

    @Throws(Exception::class)
    suspend fun getUsers(page: Int, pageSize: Int): UsersListResponse

    suspend fun getUserDetails(id:Int): UserItem

}


interface UsersRepo {

    fun getUsers(): Flow<PagingData<UserItem>>
    suspend fun getUserDetails(id: Int): Flow<ResultState<UserItem>>

}